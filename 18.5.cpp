﻿#include <iostream>

using namespace std;

class player
{
public:
	char name[256] = "name";
	int score = 0;
	void EnterName()
	{
		cin >> name >> score;
	}
	void ShowData()
	{
		cout << name << "\t" << score << "\n";
	}
};
int main()
{
	setlocale(LC_ALL, "rus");
	//Введение количества, имени и счета играков
	int size = 0;
	cout << "Введите количество играков: ";
	cin >> size;
	player* Array = new player[size];

	for (int i = 0; i < size; i++)
	{
		cout << "Введите " << i + 1 << " игрока Имя и Счет:";
		Array[i].EnterName();
	}
	cout << "\n" << "не отсортировано" << "\n";
	for (int i = 0; i < size; i++)
	{
		Array[i].ShowData();
	}
	//Сотрировка таблицы
	for (int i = 0; i < size; i++)
	{
		for (int j = size - 1; j > i; j--)
		{
			if ((Array[j].score) > (Array[j - 1].score))
			{
				swap(Array[j], Array[j - 1]);
			}
		}
	}
	cout << "\n" << "отсортировано: " << "\n";
	for (int i = 0; i < size; i++)
	{
		Array[i].ShowData();
	}
}

